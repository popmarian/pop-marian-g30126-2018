package g30126.pop.marian.l7.e3;

import java.util.Iterator;
import java.util.TreeSet;

import g30126.pop.marian.l7.e1.BankAccount;

public class BankWithTree{
		
		TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();
		TreeSet<BankAccount> accounts1 = new TreeSet<BankAccount>(new SortByOwner());
		
		public void addAccount(String owner, double balance) {
			BankAccount cont = new BankAccount(owner, balance); 
			accounts.add(cont);
			accounts1.add(cont);
		}
		
		
		
		public void printAccounts() {
			System.out.println("The accounts (sorted) are: " );
			Iterator<BankAccount> iterator = accounts.iterator();
		    while (iterator.hasNext()) {
		    	BankAccount a = iterator.next();
		    	System.out.println(a.getOwner() + " " + a.getBalance());
		    }
			
		}
		
		public void printAccounts(int min, int max) {
			Iterator<BankAccount> it  = accounts.iterator();
			System.out.println("The bank accounts between " + min + " and " + max);
			while(it.hasNext()) {
				BankAccount b = it.next();
				if(min <= b.getBalance() && max >= b.getBalance()) {
					System.out.println(b.getOwner() + " " + b.getBalance());
				}
			}
		}
		
		public BankAccount getAccount(String owner){
			Iterator<BankAccount> it = accounts1.iterator();
			while(it.hasNext()) {
				BankAccount b = it.next();
				if(b.getOwner().equals(owner)) {
				System.out.println(b.getOwner() + " " + b.getBalance());
				return b;
				}
			}
			System.out.println("Error 404: Account "+ owner + " not find!");
			return null;
		}
		
		public void getAllAcounts() {
			System.out.println("The accounts, sorted by owner, are:");
			Iterator<BankAccount> it = accounts1.iterator();
			while(it.hasNext()) {
				BankAccount a = it.next();
		    	System.out.println(a.getOwner() + " " + a.getBalance());
			}
		}
}
