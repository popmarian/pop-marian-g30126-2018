package g30126.pop.marian.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import g30126.pop.marian.l7.e1.BankAccount;

public class Bank{
	
	ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();
	
	public void addAccount(String owner, double balance) {
		BankAccount cont = new BankAccount(owner, balance); 
		accounts.add(cont);
	}
	
	
	
	public void printAccounts() {
		System.out.println("The accounts (sorted) are: ");
		Collections.sort(accounts);
		Iterator<BankAccount> iterator = accounts.iterator();
	    while (iterator.hasNext()) {
	    	BankAccount a = iterator.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
	    }
		
	}
	
	public void printAccounts(int min, int max) {
		Iterator<BankAccount> it  = accounts.iterator();
		System.out.println("The bank accounts between " + min + " and " + max);
		while(it.hasNext()) {
			BankAccount b = it.next();
			if(min <= b.getBalance() && max >= b.getBalance()) {
				System.out.println(b.getOwner() + " " + b.getBalance());
			}
		}
	}
	
	public BankAccount getAccount(String owner){
		Iterator<BankAccount> it = accounts.iterator();
		while(it.hasNext()) {
			BankAccount b = it.next();
			if(b.getOwner().equals(owner)) {
			System.out.println(b.getOwner() + " " + b.getBalance());
			return b;
			}
		}
		System.out.println("Error 404: Account "+ owner + " not find!");
		return null;
	}
	
	public void getAllAcounts() {
		System.out.println("The accounts, sorted by owner, are:");
		Collections.sort(accounts,BankAccount.OwnerNameComp);
		Iterator<BankAccount> it = accounts.iterator();
		while(it.hasNext()) {
			BankAccount a = it.next();
	    	System.out.println(a.getOwner() + " " + a.getBalance());
		}
	}
	

}
