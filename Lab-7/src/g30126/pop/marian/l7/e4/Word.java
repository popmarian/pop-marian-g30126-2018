package g30126.pop.marian.l7.e4;

public class Word {
	
	private String name;
	
	Word(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
