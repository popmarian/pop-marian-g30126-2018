package g30126.pop.marian.l7.e4;

public class Definition {
	
	private String description;
	
	Definition(String description){
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
