package g30126.pop.marian.l7.e1;

public class Main {
	public static void main(String[] args) {
	BankAccount cont1 = new BankAccount("James",200.50);
	BankAccount cont2 = new BankAccount("James", 200.50);
	BankAccount cont3 = new BankAccount("Leo", 200.50);
	
	if(cont1.equals(cont2)) {
		System.out.println("Acelasi cont.");
	}
	else {
		System.out.println("Conturi diferite.");
	}
	
	if(cont1.equals(cont3)) {
		System.out.println("Acelasi cont.");
	}
	else {
		System.out.println("Conturi diferite.");
	}
	
	cont2.withdraw(100);
	cont2.deposit(0.20);
	cont1.withdraw(100);
	
	if(cont1.equals(cont2)) {
		System.out.println("Acelasi cont.");
	}
	else {
		System.out.println("Conturi diferite.");
	}
}

}
