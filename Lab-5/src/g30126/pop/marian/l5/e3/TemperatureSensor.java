package g30126.pop.marian.l5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
	 
	 int value;
	 public TemperatureSensor() {
	  Random rand = new Random();
	  value = rand.nextInt(100);
	 }
	 public int readValue() {
	  return this.value;
	 }
	}