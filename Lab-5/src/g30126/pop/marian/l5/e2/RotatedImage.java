package g30126.pop.marian.l5.e2;

public class RotatedImage implements Image{
	String fileName;
	public RotatedImage(String fileName) {
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	
	private void loadFromDisk(String fileName) {
		System.out.println("Loading " + fileName);
	}
	
	@Override
	public void display() {
		System.out.println("Display rotated " + fileName);
	}
}
