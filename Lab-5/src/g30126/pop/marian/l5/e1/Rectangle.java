package g30126.pop.marian.l5.e1;

public class Rectangle extends Shape {
	protected double width;
	protected double length;
	
	public Rectangle(){
		this.width = 1;
		this.length = 1;
	}
	
	public Rectangle(double width, double length){
		this.width = width;
		this.length = length;
	}
	
	public Rectangle(double width, double length, String color, boolean filled) {
		this.width = width;
		this.length = length;
		this.color = color;
		this.filled = filled;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double getArea() {
		return getLength()*getWidth();
	}
	
	public double getPerimeter() {
		return 2*getLength()+2*getLength();
	}
	
	@Override
	public String toString() {
		return "A Rectangle with width =" + width +
				" length =" + length +
				" color =" + color +
				" filled =" + filled;
	}
}
