package g30126.pop.marian.l5.e1;

public class TestClass {
	public static void main(String[] args) {
		Shape[] array = new Shape[3];
		array[0] = new Circle(3);
		System.out.println(array[0].toString());
		System.out.println(array[0].getArea() + " " + array[0].getPerimeter());
		array[1] = new Rectangle(1,2);
		System.out.println(array[1].toString());
		System.out.println(array[1].getArea() + " " + array[1].getPerimeter());
		array[2] = new Square(2);
		System.out.println(array[2].toString());
		System.out.println(array[2].getArea() + " " + array[2].getPerimeter());
	}

}
