package pop.marian.test3;

public class Book {
	private int value;
	private String location;
	
	Book(){
		this.value = 0;
		this.location = null;
	}
	
	
	
	public int getValue() {
		return value;
	}



	public void setValue(int value) {
		this.value = value;
	}



	public String getLocation() {
		return location;
	}



	public void setLocation(String location) {
		this.location = location;
	}



	public String read(int value) {
		this.value = value;
		return "Sensor value is " + value;
	}
	
	public static void main(String[] args) {
		Book[] array = new Book[4];
		array[0] = new Sensor();
		array[1] = new TemperatureSensor();
		array[2] = new HumiditySensor();
		array[3] = new Book();
	}

}
