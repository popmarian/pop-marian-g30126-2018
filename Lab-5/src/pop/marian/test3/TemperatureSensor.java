package pop.marian.test3;

public class TemperatureSensor extends Book {
	
	@Override
	public String read(int value) {
		return "TemperatureSensor value is " + value;
	}

}
