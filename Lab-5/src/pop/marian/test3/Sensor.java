package pop.marian.test3;

public class Sensor extends Book {
	@Override
	public String read(int value) {
		return "Sensor value is " + value;
	}

}
