package g30126.pop.marian.l6.e6;

import java.awt.Color;
import java.awt.Graphics;

import g30126.pop.marian.l6.e1.DrawingBoard;
import g30126.pop.marian.l6.e1.Shape;



public class Fractal extends Shape {
	
	private int length;
	private int width;
	
	Fractal(Color color, int length, int width){
		super(color);
		this.length = length;
		this.width = width;
	}
	
	@Override
	public void draw(Graphics g) {
		if(length>5 && width>5) {
		g.drawRect(DrawingBoard.pixelix/2-length/2, DrawingBoard.pixeliy/2-width/2, length, width);
		length = length-20;
		width = width-20;
		draw(g);
		}
		
	}
	
	public static void main(String[] args) {
		DrawingBoard b = new DrawingBoard();
		Shape f = new Fractal(Color.RED, 500, 500);
		b.addShape(f);
		
	}
}
