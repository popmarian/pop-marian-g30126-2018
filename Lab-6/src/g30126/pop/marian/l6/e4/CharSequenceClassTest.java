package g30126.pop.marian.l6.e4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CharSequenceClassTest {
	
	char[] car = {'v', 'w', 'x', 'y', 'z'};
	CharSequenceClass cr = new CharSequenceClass(car);
	
	@Test
	void testCharAt() {
		assertEquals('x',cr.charAt(2));
	}

	@Test
	void testLength() {
		assertEquals(5,cr.length());
	}

	@Test
	void testSubSequence() {
		assertEquals("xyz",cr.subSequence(2, 5));
	}

}
