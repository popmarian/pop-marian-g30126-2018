package g30126.pop.marian.l6.e4;

import java.lang.*;

public class CharSequenceClass implements CharSequence{
	
	private char[] chars = new char[20];
	
	CharSequenceClass(char[] chars) {
		this.chars = chars;
	}
	
	@Override
	public char charAt(int index) {
		return chars[index];
	}

	@Override
	public int length() {
		return chars.length;
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		String subsequence = new String();
		for(int i = start; i<end; i++) {
			subsequence = subsequence + chars[i];
		}
			return subsequence;
	}
	
	public static void main(String[] args) {
		char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
		CharSequenceClass ch = new CharSequenceClass(chars);
		System.out.println(ch.charAt(3));
		System.out.println(ch.length());
		System.out.println(ch.subSequence(1, 5));
	}
	
}
