package g30126.pop.marian.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public abstract class AbstractShape implements Shape {
	private int x, y;
    private Color color;
    private String id;
    private boolean fill;

    public AbstractShape(Color color, int x, int y, String id, boolean fill) {
        this.x = x;
        this.y = y;
    	this.color = color;
    	this.id = id;
    	this.fill = fill;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public boolean isFill() {
		return fill;
	}

	public void setFill(boolean fill) {
		this.fill = fill;
	}

	public abstract void draw(Graphics g);
}