package g30126.pop.marian.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
	
	void draw(Graphics g);

}

