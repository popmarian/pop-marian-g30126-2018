package g30126.pop.marian.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

import g30126.pop.marian.l6.e1.Shape;
import g30126.pop.marian.l6.e1.DrawingBoard;;

public class Rectangle extends Shape{

   private int length;
   private int width;
   
   public Rectangle(Color color, int length, int width) {
	   super(color);
	   this.length = length;
	   this.width = width;
   }

   public Rectangle(Color color, int length, int width, int x, int y, String id, boolean fill) {
       super(color,x,y,id,fill);
       this.length = length;
       this.width = width;
   }

   @Override
   public void draw(Graphics g) {
       System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
       g.setColor(getColor());
       if(isFill() == false) {
       	g.drawRect(getX(), getY(), length, width);
       }
       else 
       	g.fillRect(getX(), getY(), length, width);
   }
}