package g30126.pop.marian.l6.e5;

import java.awt.Color;
import java.awt.Graphics;

import g30126.pop.marian.l6.e1.DrawingBoard;
import g30126.pop.marian.l6.e1.Shape;

public class Pyramid extends Rectangle{
	private int bricks;
	private int length;
	private int width;


	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public Pyramid(Color color, int length, int width, int bricks){
		super(color, length, width);
		this.bricks = bricks; //numarul de caramizi introduse de utilizator
		this.length = length;
		this.width = width;
	}

	@Override
	public void draw(Graphics g) {
		System.out.println("Drawing the highest possible pyramid with " + bricks + " bricks.");
		int nr = 0;
		int p;
		int i = 0;
		int linie, coloana;
		while(i+nr<bricks) {
			i++; //numarul de caramizi 
			nr = nr + i; 
			p=i;
			while(p !=  0) {

				for(linie = 0; linie<=i;linie++) {
					for(coloana=0; coloana <linie; coloana++) {
						g.drawRect(((DrawingBoard.pixelix)/2 - (linie*(getLength()/2) - (coloana*getLength()))),(40 + linie*getWidth()) , getLength(), getWidth());
					}
				}
				p--;
			}
		}
	}


	public static void main(String[] args) {
		DrawingBoard b = new DrawingBoard();

		Shape p = new Pyramid(Color.RED, 100, 50, 56);
		b.addShape(p);
		System.out.println();

	}
}
