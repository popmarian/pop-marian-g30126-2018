package g30126.pop.marian.l6.e1;


import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90, 80, 35,"1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100, 10, 40,"2",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 100, 90, 40,"3",true);
        b1.addShape(s3);
        //b1.deleteByld("3");
    }
}