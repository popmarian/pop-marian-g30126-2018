package g30126.pop.marian.l6.e1;



import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard  extends JFrame {

    Shape[] shapes = new Shape[10];
    //ArrayList<Shape> shapes = new ArrayList<>();
    public static final int pixelix = 1000;
    public static final int pixeliy = 1000;
    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(pixelix,pixeliy);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }


	public void addShape(Shape s1){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]==null){
                shapes[i] = s1;
                break;
            }
        }
//        shapes.add(s1);
        this.repaint();
    }
    
    public void deleteByld(String id) {
    	for(int i = 0; i < shapes.length; i++) {
    	if(shapes[i] != null && shapes[i].getId().equals(id)) {
    		shapes[i] = null;
    	}
    	}
    	repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<shapes.length;i++){
            if(shapes[i]!=null)
                shapes[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}