package g30126.pop.marian.l4.e7;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {

	@Test
	public void testGetArea() {
		System.out.println("Execute test 1");
		Cylinder c = new Cylinder(1,1);
		assertEquals(c.getArea(),12.56,0.01);
	}

	@Test
	public void testGetVolume() {
		System.out.println("Execute test 2");
		Cylinder c = new Cylinder(1,1);
		assertEquals(c.getVolume(),3.14,0.01);
	}

}
