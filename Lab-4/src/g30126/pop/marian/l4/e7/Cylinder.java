package g30126.pop.marian.l4.e7;

import g30126.pop.marian.l4.e3.Circle;

public class Cylinder extends Circle {
	private double height;
	
	public Cylinder() {
		super();
	}
	
	public Cylinder(double radius) {
		super(radius);
	}
	
	public Cylinder(double radius, double height) {
		super(radius);
		this.height = height;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getVolume() {
		return Math.PI*(getRadius()*getRadius())*height;
	}
	
	public double getArea() {
		return 2*Math.PI*getRadius()*(getRadius()+height);
	}
	
	public static void main(String[] args) {
		Cylinder c = new Cylinder(1,1);
		System.out.println(c.getVolume());
		System.out.println(c.getArea());
	}
}
