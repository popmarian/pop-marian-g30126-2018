package g30126.pop.marian.l4.e9;

import becker.robots.*;

public class Diver {
	private int x;
	private int y;
	private Robot Karel;
	private City Brasov;
	
	public Diver(int x, int y) {
		this.x = x;
		this.y = y;
		this.Brasov = new City();
		this.Karel = new Robot(Brasov,x,y,Direction.NORTH);
		
		Wall zid0 = new Wall(Brasov,x+1,y,Direction.NORTH);
		Wall zid1 = new Wall(Brasov,x+1,y-1,Direction.EAST);
		Wall zid2 = new Wall(Brasov,x+2,y-1,Direction.EAST);
		Wall zid3 = new Wall(Brasov,x+3,y-1,Direction.EAST);
		Wall zid4 = new Wall(Brasov,x+3,y,Direction.WEST);
		Wall zid5 = new Wall(Brasov,x+3,y,Direction.SOUTH);
		Wall zid6 = new Wall(Brasov,x+3,y+1,Direction.SOUTH);
		Wall zid7 = new Wall(Brasov,x+3,y+2,Direction.WEST);
	}
	
	public void Dive() {
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.move();
		Karel.move();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
	}
	public static void main(String[] args) {
		Diver Karel = new Diver(2,2);
		Karel.Dive();
	}
}
