package g30126.pop.marian.l4.e8;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShapeTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1");
		Shape s = new Shape("red", true);
		assertEquals("A Shape with color of red and filled true",s.toString());
	}

}
