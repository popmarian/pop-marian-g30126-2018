package g30126.pop.marian.l4.e6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import g30126.pop.marian.l4.e4.Author;

class BookTest {

	@Test
	void testToString() {
		System.out.println("Execute test 1.");
		Author[] a;
		Book b = new Book("Antimateria", a = new Author[3],50.34,1000);
		a[0] = new Author("Ion", "ion@email.com",'m');
		a[1] = new Author("Gheo", "gheo@yahoo.com",'m');
		a[2] = new Author("Maria", "gheo@yahoo.com",'m');
		assertEquals(b.toString(),"'book-Antimateria' by 3 authors");
		
	}

	@Test
	void testPrintAuthors() {
		Author[] a;
		Book b = new Book("Antimateria", a = new Author[3],50.34,1000);
		a[0] = new Author("Ion", "ion@email.com",'m');
		a[1] = new Author("Gheo", "gheo@yahoo.com",'m');
		a[2] = new Author("Maria", "gheo@yahoo.com",'m');
		
		assertEquals("Ion",a[0].getName());
		assertEquals("Gheo",a[1].getName());
		assertEquals("Maria",a[2].getName());

	}

}
