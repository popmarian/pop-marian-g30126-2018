package g30126.pop.marian.l4.e4;

public class Author {
	
	private String name;
	private String email;
	private char gender;
	
	Author(){
		name = "your name";
		email = "youremail@example.com";
		gender = 'm';
	}
	
	public Author(String name, String email, char gender) {
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	
	
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public char getGender() {
		return gender;
	}
	
	public String toString() {
		return "author-" + name + "(" + gender + ") at " + email; 
	}

	public static void main(String[] args) {
		Author a1 = new Author();
		System.out.println(a1.toString());
		a1.setEmail("p.marian@yahoo.com");
		System.out.println(a1.getEmail() + "\n" + a1.getName() + "\n" + a1.getGender());
		
		Author a2 = new Author("Pop Marian", "pop.marian@yahoo.com", 'm');
		System.out.println(a2.toString());
	}

}
