package g30126.pop.marian.l3.e5;

import becker.robots.*;

public class FetchingTheNewspaper {
	public static void main(String[]args) {
		City Sokovia = new City();
		Wall zid1 = new Wall(Sokovia,1,1,Direction.NORTH);
		Wall zid2 = new Wall(Sokovia,1,2,Direction.NORTH);
		Wall zid3 = new Wall(Sokovia,1,2,Direction.EAST);
		Wall zid4 = new Wall(Sokovia,1,2,Direction.SOUTH);
		Wall zid5 = new Wall(Sokovia,1,1,Direction.WEST);
		Wall zid6 = new Wall(Sokovia,2,1,Direction.WEST);
		Wall zid7 = new Wall(Sokovia,2,1,Direction.SOUTH);
		Thing newspaper = new Thing(Sokovia,2,2);
		Robot Karel = new Robot(Sokovia,1,2,Direction.SOUTH);
		
		//Karel is going to take the newspaper
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.move();
		Karel.pickThing();
		
		//Karel is going back to bed
		
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.move();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.turnLeft();
		Karel.putThing();
	}

}
