package g30126.pop.marian.l3.e3;

import becker.robots.*;

public class RobotReturn {
	public static void main(String[]args) {
		City Bucuresti =new City();
		Robot Jon = new Robot(Bucuresti, 1, 1, Direction.NORTH);
		
		//Jon is moving NORTH 5 times
		
		Jon.move();
		Jon.move();
		Jon.move();
		Jon.move();
		Jon.move();
		
		//Jon is turning around and returning to he's starting point
		
		Jon.turnLeft();
		Jon.turnLeft();
		
		Jon.move();
		Jon.move();
		Jon.move();
		Jon.move();
		Jon.move();
	}

}
