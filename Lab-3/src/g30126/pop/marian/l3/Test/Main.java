package g30126.pop.marian.l3.Test;

import java.util.Arrays;

public class Main {

    public static int solution(int[] A){
    	Arrays.sort(A);
    	int result=0;
    	int i=0;
    	if(A[i]!=A[i+1])
    		{
    			return A[i];
    		}
    	else {	
    		i++;
    		while (i<A.length-1 ){
    			if(A[i] != A[i-1] && A[i]!=A[i+1]){
       				result=A[i];
    				}
    			i++;
    			}
    		 }
    	if(i==A.length-1 && A[i]!=A[i-1]){
    		result=A[i];
    	}
    	return result;
    }
    
    
    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
        int result = solution(A);
        
        System.out.println(result);
        
        if(result==7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
    
    
}

