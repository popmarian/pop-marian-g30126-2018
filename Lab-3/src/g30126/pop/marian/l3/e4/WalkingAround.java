package g30126.pop.marian.l3.e4;

import becker.robots.*;

public class WalkingAround {
	static public void main(String[]args) {
	City Las_Fierbinti = new City();
	Wall gard1 = new Wall(Las_Fierbinti,1,1,Direction.NORTH);
	Wall gard2 = new Wall(Las_Fierbinti,1,2,Direction.NORTH);
	Wall gard3 = new Wall(Las_Fierbinti,1,2,Direction.EAST);
	Wall gard4 = new Wall(Las_Fierbinti,2,2,Direction.EAST);
	Wall gard5 = new Wall(Las_Fierbinti,2,2,Direction.SOUTH);
	Wall gard6 = new Wall(Las_Fierbinti,2,1,Direction.SOUTH);
	Wall gard7 = new Wall(Las_Fierbinti,2,1,Direction.WEST);
	Wall gard8 = new Wall(Las_Fierbinti,1,1,Direction.WEST);
	Robot Gheo = new Robot(Las_Fierbinti,0,2,Direction.WEST);
	
	//Gheo is making sport
	
	Gheo.move();
	Gheo.move();
	Gheo.turnLeft();
	Gheo.move();
	Gheo.move();
	Gheo.move();
	Gheo.turnLeft();
	Gheo.move();
	Gheo.move();
	Gheo.move();
	Gheo.turnLeft();
	Gheo.move();
	Gheo.move();
	Gheo.move();
	Gheo.turnLeft();
	Gheo.move();
	}
}
