package g30126.pop.marian.l3.e6;

import java.lang.Math;

public class MyPoint {
	private int x;
	private int y;
	
	MyPoint() {
		x=0;
		y=0;
		
	}
	
	MyPoint(int x,int y){
		this.x=x;
		this.y=y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public String toString() {
		String s = "(" + getX() + "," + getY() +")";
		return s;
	}
	
	public double distance(int x, int y) {
		return Math.sqrt((x - getX())*(x - getX())+(y-getY())*(y-getY()));
	}
	
	public double distance(MyPoint punct) {
		return Math.sqrt((punct.getX()- getX())*(punct.getX() - getX())+(punct.getY()-getY())*(punct.getY()-getY()));
	}
	
	public static void main(String[] args) {
		MyPoint p = new MyPoint();
		p.setX(1);
		p.setY(1);
		System.out.println(p.toString());
		System.out.println(p.distance(2, 2));
		MyPoint punctulet = new MyPoint(1,1);
		System.out.println(p.distance(punctulet));
		}

}
