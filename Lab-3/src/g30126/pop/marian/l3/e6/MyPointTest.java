package g30126.pop.marian.l3.e6;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyPointTest {
	
	private MyPoint punct = new MyPoint(1,1);
	private MyPoint punct1 = new MyPoint();
	

	@Test
	void testMyPoint() {
		System.out.println("Executing test 1");
		assertEquals(punct1.getX(),0);
		assertEquals(punct1.getY(),0);
	}

	@Test
	void testMyPointintint() {
		System.out.println("Executing test 2");
		assertEquals(punct.getX(),1);
		assertEquals(punct.getY(),1);
	}

	@Test
	void testGetX() {
		System.out.println("Executing test 3");
		assertEquals(punct.getX(),1);
	}

	@Test
	void testSetX() {
		System.out.println("Executing test 4");
		punct.setX(1);
		assertEquals(punct.getX(),1);
	}

	@Test
	void testGetY() {
		System.out.println("Executing test 5");
		assertEquals(punct.getY(),1);
	}
	

	@Test
	void testSetY() {
		System.out.println("Executing test 6");
		punct.setY(1);
		assertEquals(punct.getY(),1);
	}

	@Test
	void testSetXY() {
		System.out.println("Executing test 7");
		punct.setX(1);
		punct.setY(1);
		assertEquals(punct.getX(),1);
		assertEquals(punct.getY(),1);
	}

	@Test
	void testToString() {
		System.out.println("Executing test 8");
		assertEquals(punct.toString(),"(1,1)");
	}

	@Test
	void testDistanceIntInt() {
		System.out.println("Executing test 9");
		punct.setX(4);
		punct.setY(4);
		assertEquals(punct.distance(4,4),0);
	}

	@Test
	void testDistanceMyPoint() {
		System.out.println("Executing test 10");
		punct.setX(4);
		punct.setY(4);
		punct1.setXY(4, 4);
		assertEquals(punct.distance(punct1),0);
	}

}
