package g30126.pop.marian.l8.e1;

class ConcentrationException extends Exception{
	int c;
	public ConcentrationException(int c,String msg) {
		super(msg);
		this.c = c;
	}

	int getConc(){
		return c;
	}
}//.class