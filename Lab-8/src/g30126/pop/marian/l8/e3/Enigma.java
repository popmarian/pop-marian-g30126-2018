package g30126.pop.marian.l8.e3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Enigma{
	
	
	private String fileName;
	
	public void citireFisier() throws IOException {
		System.out.println("Introduceti numele fisierului(calea completa): ");
		BufferedReader bufin =  new BufferedReader(new InputStreamReader(System.in));
		setFileName(bufin.readLine());
	}


	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void encrypt() throws IOException {
		citireFisier();
		BufferedReader fileread = new BufferedReader(new FileReader(getFileName()));
		PrintWriter filewrite = new PrintWriter (new BufferedWriter (new FileWriter(getFileName()+".enc")));
		String sin = new String();
		while((sin = fileread.readLine()) != null) {
			String sout = new String();
			for(char ch : sin.toCharArray()) {
				ch = (char)(ch - 1);
				sout = sout.substring(0)+ch;
			}
			filewrite.println(sout);
			
		}
		filewrite.close();
		fileread.close();
		
	}
	
	public void decrypt() throws IOException {
		citireFisier();
		BufferedReader fileread = new BufferedReader(new FileReader(getFileName()));
		PrintWriter filewrite = new PrintWriter (new BufferedWriter (new FileWriter(getFileName()+".dec")));
		String sin = new String();
		while((sin = fileread.readLine()) != null) {
			String sout = new String();
			for(char ch : sin.toCharArray()) {
				ch = (char)(ch + 1);
				sout = sout.substring(0)+ch;
			}
			filewrite.println(sout);
			
		}
		filewrite.close();
		fileread.close();
		
	}

	
	
	
}
