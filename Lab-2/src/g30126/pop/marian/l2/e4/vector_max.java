package g30126.pop.marian.l2.e4;

import java.util.Random;
import java.util.Arrays;

public class vector_max {
	public static void main(String[]args) {
		Random a = new Random();
		int N = 100, r;
		int[] vector = new int [N];
		for(int i=0;i<N;i++) {
			vector[i] = a.nextInt();
		}
		System.out.println(Arrays.toString(vector));
		r = elem_vector_max(vector);
		System.out.println("Maximum element of of the vector is: " + r);
	}
	
	static int elem_vector_max(int[] vector) {
		int max=0;
		for(int i=0;i<vector.length;i++) {
			if(max < vector[i]) 
				 max=vector[i];
			else max = max;
		}
		return max;
	}

}
