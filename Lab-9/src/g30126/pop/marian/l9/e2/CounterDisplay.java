package g30126.pop.marian.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class CounterDisplay extends JFrame {
	
	JButton bCounter;
	JTextArea textA;
	JTextField textF;
	JLabel counter;
	
	
	CounterDisplay(){
		setTitle("Counter Display");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initializare();
		setSize(500,500);
		setVisible(true);
	}
	
	public void initializare() {
		this.setLayout(null);
		int width = 80; int height = 20;
		
		counter = new JLabel("Counter");
		counter.setBounds(10, 50, width, height);
		
//		textA = new JTextArea();
//		textA.setBounds(60, 50, width, height);
		
		textF = new JTextField();
		textF.setBounds(60, 50, width, height);
		
		bCounter = new JButton("Count");
		bCounter.setBounds(150, 50, width, height);
		bCounter.addActionListener(new TratareButonCounter());
		
		add(counter);
//		add(textA);
		add(bCounter);
		add(textF);
	}
	
	public static void main(String[] args) {
		CounterDisplay cd = new CounterDisplay();
	}
	
	public class TratareButonCounter implements ActionListener{
		
		private int counter = 0;
		
		public int getCounter() {
			return counter;
		}

		@Override
		public void actionPerformed(ActionEvent me) {
			counter++;
			textF.setText(String.valueOf(counter));
		}

	}

}
