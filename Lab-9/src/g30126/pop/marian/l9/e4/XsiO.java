package g30126.pop.marian.l9.e4;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.*;

public class XsiO extends JFrame{

	JButton b1;
	JButton b2;
	JButton b3;
	JButton b4;
	JButton b5;
	JButton b6;
	JButton b7;
	JButton b8;
	JButton b9;
	JTextArea textA;

	private boolean b = true;

	ArrayList<JButton> buttons = new ArrayList<JButton>();

	int [] sir = {2,2,2,2,2,2,2,2,2}; 


	XsiO(){
		setTitle("Joc XsiO");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initializare();
		setSize(500,500);
		setVisible(true);
	}

	public void initializare() {
		this.setLayout(null);

		int width = 50; int height = 50;

		textA = new JTextArea();
		textA.setBounds(50, 200, 200, 300);
		textA.setEditable(false);

		b1 = new JButton();
		b1.setBounds(50, 50, width, height);

		b2 = new JButton();
		b2.setBounds(100, 50, width, height);

		b3 = new JButton();
		b3.setBounds(150, 50, width, height);

		b4 = new JButton();
		b4.setBounds(50, 100, width, height);

		b5 = new JButton();
		b5.setBounds(100, 100, width, height);

		b6 = new JButton();
		b6.setBounds(150, 100, width, height);

		b7 = new JButton();
		b7.setBounds(50, 150, width, height);

		b8 = new JButton();
		b8.setBounds(100, 150, width, height);

		b9 = new JButton();
		b9.setBounds(150, 150, width, height);

		buttons.add(b1);buttons.add(b2);buttons.add(b3);
		buttons.add(b4);buttons.add(b5);buttons.add(b6);
		buttons.add(b7);buttons.add(b8);buttons.add(b9);

		add(textA);

		Iterator<JButton> it = buttons.iterator();
		while(it.hasNext()) {
			JButton bi = it.next();
			add(bi);
			bi.addActionListener(new TratareButon());
		}
	}

	public static void main(String[] args) {
		XsiO joc = new XsiO();
	}

	public class TratareButon implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent ae) {
			for(JButton bi : buttons){
				if(b && ae.getSource().equals(bi)) {
					bi.setText("X");
					sir[buttons.indexOf(bi)] = 1;
					((JButton) ae.getSource()).setEnabled(false);
				}
				if(!b && ae.getSource().equals(bi)){
					bi.setText("O");
					sir[buttons.indexOf(bi)] = 0;
					((JButton) ae.getSource()).setEnabled(false);

				}
				boolean conditieX = (((sir[0]==1) && (sir[1]==1) && (sir[2]==1)) ||
						((sir[3]==1) && (sir[4]==1) && (sir[5]==1)) ||
						((sir[6]==1) && (sir[7]==1) && (sir[8]==1)) ||
						((sir[0]==1) && (sir[3]==1) && (sir[6]==1)) ||
						((sir[1]==1) && (sir[4]==1) && (sir[7]==1)) ||
						((sir[2]==1) && (sir[5]==1) && (sir[8]==1)) ||
						((sir[0]==1) && (sir[4]==1) && (sir[8]==1)) ||
						((sir[2]==1) && (sir[4]==1) && (sir[6]==1)));
				boolean conditie0 = ((sir[0]==0 && sir[1]==0 && sir[2]==0) ||
						(sir[3]==0 && sir[4]==0 && sir[5]==0) ||
						(sir[6]==0 && sir[7]==0 && sir[8]==0) ||
						(sir[0]==0 && sir[3]==0 && sir[6]==0) ||
						(sir[1]==0 && sir[4]==0 && sir[7]==0) ||
						(sir[2]==0 && sir[5]==0 && sir[8]==0) ||
						(sir[0]==0 && sir[4]==0 && sir[8]==0) ||
						(sir[4]==0 && sir[2]==0 && sir[6]==0));

				if(conditieX==true) {
					textA.setText("WINNER\n X, YOU ARE THE BOSS!");	
					WinnerWindow w1 = new WinnerWindow();
					XsiO.this.disable();

				} else if(conditie0==true) {
					textA.setText("WINNER\n 0, YOU ARE THE HERO!");
					WinnerWindow w1 = new WinnerWindow();
					XsiO.this.disable();
				}
			}
			b=!b;
		}


	}

	public class WinnerWindow extends JFrame{
		JButton bt1;
		JButton bt2;

		WinnerWindow(){
			setTitle("Winner Menu");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			initializare();
			setSize(500,700);
			setVisible(true);
		}

		public void initializare() {
			this.setLayout(null);

			int height = 30; int width = 100;

			bt1 = new JButton("Restart");
			bt1.setBounds(167, 50, width, height);
			bt1.addActionListener(new TratareButonWW());

			bt2 = new JButton("Close");
			bt2.setBounds(334, 50, width, height);
			bt2.addActionListener(new TratareButonWW());

			add(bt1); add(bt2);
		}

		public class TratareButonWW implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {

				if(e.getSource().equals(bt1)) {

				}

				if(e.getSource().equals(bt2)) {
					System.exit(0);
				}

			}

		}
	}

}

