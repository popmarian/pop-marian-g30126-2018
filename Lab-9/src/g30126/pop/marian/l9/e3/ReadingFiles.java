package g30126.pop.marian.l9.e3;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.io.*;

public class ReadingFiles extends JFrame {
	
	JButton button;
	JTextField textF;
	JTextArea textA;
	JLabel nameL;
	JLabel nameL1;
	
	ReadingFiles(){
		setTitle("File Reader");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		initializare();
		setSize(1000,1000);
		setVisible(true);
	}
	
	
	public void initializare() {
		this.setLayout(null);

		nameL = new JLabel("Introduceti numele fisierului:");
		nameL.setBounds(10, 40, 500, 10);
		
		textF = new JTextField();
		textF.setBounds(10, 60, 500, 20);
		
		textA = new JTextArea();
		textA.setBounds(10, 100, 1000, 1000);
		
		nameL1 = new JLabel("Continutul fisierului:");
		nameL1.setBounds(10, 80, 500, 10);
		
		button = new JButton("Citeste fisierul");
		button.setBounds(520, 60, 150, 20);
		button.addActionListener(new TratareButon());
		
		add(nameL);
		add(textF);
		add(nameL1);
		add(textA);
		add(button);
	}
	
	public static void main(String[] args) {
		ReadingFiles rf = new ReadingFiles();
	}
	
	public class TratareButon implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			String fisier = ReadingFiles.this.textF.getText();
			
			try {
				BufferedReader bufin = new BufferedReader(new FileReader(fisier));
				String linie = bufin.readLine();
					while(linie != null) {
						ReadingFiles.this.textA.append(linie+"\n");
						linie = bufin.readLine();
					}
					bufin.close();	
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
			
			
			
		}
		
	}

}
