package g30126.pop.marian.l10.e3;

public class SeparetedCounters extends Thread{

	private int type;
	Thread thr;

	public int getType() {
		return type;
	}

	SeparetedCounters(String name,Thread thr){
		super(name);
		this.thr = thr;
	}

	
	public void run() {
		for(int i = 0,j = 0; i<=200 || j<=100; i++, j++) {
			try {
				if(thr!= null && i>100) {
				thr.join();
				System.out.println(getName() + " i = " + i);
			}else if(thr==null && i<=100){
				System.out.println(getName() + " i = " + j);
			}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Gata treaba!");
	}
	
	public static void main(String[] args) {
		SeparetedCounters c1 = new SeparetedCounters("C1",null);
		SeparetedCounters c2 = new SeparetedCounters("C2",c1);
		
		c1.start();
		c2.start();
	}

}
