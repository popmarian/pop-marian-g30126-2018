package g30126.pop.marian.l10.e4;

import java.util.ArrayList;
import java.util.Observable;

public class Space2D extends Observable{

	static ArrayList<Robotzi> roboti = new ArrayList<Robotzi>();
	static Robotzi[][] poz = new Robotzi[100][100];

	public void listAdd() {
		for(int i = 0; i<10; i++) {
			roboti.add(new Robotzi());
		}
	}
	static public void addRobo() {
		for(int i = 0; i<100; i++) {
			for(int j = 0; j<100; j++) {
				for(Robotzi r : roboti) {
					if(i == r.getX() && j == r.getY()) {
						poz[i][j] = r;
					}
				}
			}
		}
	}

	public void displayMoves() {
		for(Robotzi r : roboti) {
			r.start();
		}
	}

	public static void main(String[] args) {

		//		Robotzi robi = new Robotzi();
		//		System.out.println(robi.getX() + " " + robi.getY());
		//		Robotzi robi1 = new Robotzi();
		//		System.out.println(robi1.getX() + " " + robi1.getY());
		//		Robotzi robi2 = new Robotzi();
		//		System.out.println(robi2.getX() + " " + robi2.getY());
		//		robi2.start();
		//		robi.start();
		//		robi1.start();

		Space2D s = new Space2D();
		s.listAdd();
		s.displayMoves();
	}
}
